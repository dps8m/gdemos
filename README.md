Extracts from Tavares' gdemos archive.

cage/

    The main program bird.fortran is missing lines at the beginning and does
    not appear to do anything.

cribbage/

    Works. Patched up to be bindable. Call to get_mydir_ replaced with get_wdir,
    so the "cribbage.pgs" segment needs to be in the working directory 
    instead of the installation directory.  A Makefile was added.

demos/

    cycloids.pl1 -- works.
    display_config_deck.pl1 -- Works; fixed to match updated FNP config deck
       data. 
    gchars.pl1 -- missing "concatenate_args_".
    graphic_who.pl1 -- Works.
    gttm.pl1 -- Refers to depracated "rws" data in the SST; commented out.
       Other then that, appears to work.
    gwcm.pl1 -- Works; needs ring0 access.
    test_graphic_input.pl1 -- Works.

globe/

   Added "convert.pl1" to build segment "world.bin" from "globe.coords".

gomoku/

   Works.

pictures/

   "airstruc.ge" does not seem to work. The other segments have a 
   ".graphics" suffix and appear to contain graphics data; it is not
   known how to draw the data.



